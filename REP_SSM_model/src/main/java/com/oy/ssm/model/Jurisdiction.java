package com.oy.ssm.model;

import java.io.Serializable;

public class Jurisdiction implements Serializable {
    private Integer jurisdictionId;

    private String jurisdictionName;

    private String jurisdictionDescribe;

    private Integer jurisdictionStatic;

    private static final long serialVersionUID = 1L;

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public String getJurisdictionName() {
        return jurisdictionName;
    }

    public void setJurisdictionName(String jurisdictionName) {
        this.jurisdictionName = jurisdictionName == null ? null : jurisdictionName.trim();
    }

    public String getJurisdictionDescribe() {
        return jurisdictionDescribe;
    }

    public void setJurisdictionDescribe(String jurisdictionDescribe) {
        this.jurisdictionDescribe = jurisdictionDescribe == null ? null : jurisdictionDescribe.trim();
    }

    public Integer getJurisdictionStatic() {
        return jurisdictionStatic;
    }

    public void setJurisdictionStatic(Integer jurisdictionStatic) {
        this.jurisdictionStatic = jurisdictionStatic;
    }
}