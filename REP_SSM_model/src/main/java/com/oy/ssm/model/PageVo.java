package com.oy.ssm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 勋
 * @ClassName PageVo
 * @Description
 * @Date 2021-07-13 09:53:53
 * @Version v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageVo<T> {
    public Long total;
    public List<T> rows;
}
