package com.oy.ssm.model;

import java.io.Serializable;

public class Detail implements Serializable {
    private Integer detailId;

    private Integer jurisdictionId;

    private String detailWay;

    private Integer detailStatic;

    private static final long serialVersionUID = 1L;

    public Integer getDetailId() {
        return detailId;
    }

    public void setDetailId(Integer detailId) {
        this.detailId = detailId;
    }

    public Integer getJurisdictionId() {
        return jurisdictionId;
    }

    public void setJurisdictionId(Integer jurisdictionId) {
        this.jurisdictionId = jurisdictionId;
    }

    public String getDetailWay() {
        return detailWay;
    }

    public void setDetailWay(String detailWay) {
        this.detailWay = detailWay == null ? null : detailWay.trim();
    }

    public Integer getDetailStatic() {
        return detailStatic;
    }

    public void setDetailStatic(Integer detailStatic) {
        this.detailStatic = detailStatic;
    }
}