package com.oy.ssm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private Integer userId;

    private String userName;

    private String userPassword;

    private Integer userJurisdictionId;

    private Integer userStatic;

    private Jurisdiction jurisdiction;

    private static final long serialVersionUID = 1L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public Integer getUserJurisdictionId() {
        return userJurisdictionId;
    }

    public void setUserJurisdictionId(Integer userJurisdictionId) {
        this.userJurisdictionId = userJurisdictionId;
    }

    public Integer getUserStatic() {
        return userStatic;
    }

    public void setUserStatic(Integer userStatic) {
        this.userStatic = userStatic;
    }
}