package com.oy.ssm.mapper;

import com.oy.ssm.model.Jurisdiction;

import java.util.List;

public interface JurisdictionMapper {
    int deleteByPrimaryKey(Integer jurisdictionId);

    int insert(Jurisdiction record);

    int insertSelective(Jurisdiction record);

    Jurisdiction selectByPrimaryKey(Integer jurisdictionId);

    int updateByPrimaryKeySelective(Jurisdiction record);

    int updateByPrimaryKey(Jurisdiction record);

    List<Jurisdiction> selectList(Jurisdiction jurisdiction);
}