package com.oy.ssm.mapper;

import com.oy.ssm.model.Detail;

import java.util.List;

public interface DetailMapper {
    int deleteByPrimaryKey(Integer detailId);

    int insert(Detail record);

    int insertSelective(Detail record);

    Detail selectByPrimaryKey(Integer detailId);

    int updateByPrimaryKeySelective(Detail record);

    int updateByPrimaryKey(Detail record);

    List<Detail> selectList(Detail detail);
}