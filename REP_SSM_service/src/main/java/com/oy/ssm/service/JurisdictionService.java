package com.oy.ssm.service;

import com.github.pagehelper.PageInfo;
import com.oy.ssm.model.Jurisdiction;

import java.util.List;
import java.util.Map;

/**
 * @author 勋
 * @ClassName JurisdictionService
 * @Description
 * @Date 2021-07-13 09:33:09
 * @Version v1.0
 */
public interface JurisdictionService {
    /**
     * 根据条件查询数据
     * @param jurisdiction 条件
     * @return 符合条件的数据
     */
    List<Jurisdiction> selectList(Jurisdiction jurisdiction);

    /**
     * 查询符合条件的数据并分页
     * @param page 查询的页数
     * @param size 显示的数据条数
     * @param jurisdiction 条件
     * @return 符合条件的分页数据
     */
    PageInfo<Jurisdiction> selectPage(int page,int size,Jurisdiction jurisdiction);

    /**
     * 查询符合条件的数据并分页
     * @param map 需要的数据
     * @return 符合条件的分页数据
     */
    PageInfo<Jurisdiction> selectPage(Map<String,String> map);

    /**
     * 根据主键id查询单个数据
     * @param id 主键
     * @return 符合条件的数据
     */
    Jurisdiction selectKey(int id);

    /**
     * 添加数据
     * @param jurisdiction 数据
     * @return 0-没有这条数据，1-成功
     */
    int insert(Jurisdiction jurisdiction);

    /**
     * 修改数据
     * @param jurisdiction 数据
     * @return 0-没有这条数据，1-成功
     */
    int update(Jurisdiction jurisdiction);

    /**
     * 删除数据
     * @param id 主键id
     * @return 0-没有这条数据，1-成功
     */
    int delete(int id);

}
