package com.oy.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.oy.ssm.common.GetObject;
import com.oy.ssm.mapper.JurisdictionMapper;
import com.oy.ssm.model.Jurisdiction;
import com.oy.ssm.service.JurisdictionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 勋
 * @ClassName JurisdictionServiceImpl
 * @Description
 * @Date 2021-07-13 09:33:26
 * @Version v1.0
 */
@Service
public class JurisdictionServiceImpl implements JurisdictionService {
    @Autowired
    JurisdictionMapper jurisdictionMapper;

    @Override
    public List<Jurisdiction> selectList(Jurisdiction jurisdiction) {
        return jurisdictionMapper.selectList(jurisdiction);
    }

    @Override
    public PageInfo<Jurisdiction> selectPage(int page, int size, Jurisdiction jurisdiction) {
        PageHelper.startPage(page,size);
        List<Jurisdiction> jurisdictions = selectList(jurisdiction);
        return new PageInfo<>(jurisdictions);
    }

    @Override
    public PageInfo<Jurisdiction> selectPage(Map<String, String> map) {
        PageHelper.startPage(Integer.valueOf(map.get("page")),Integer.valueOf(map.get("size")));
        Jurisdiction webObject = GetObject.getWebObject(map, Jurisdiction.class);
        List<Jurisdiction> jurisdictions = selectList(webObject);
        return new PageInfo<>(jurisdictions);
    }

    @Override
    public Jurisdiction selectKey(int id) {
        return jurisdictionMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insert(Jurisdiction jurisdiction) {
        return jurisdictionMapper.insertSelective(jurisdiction);
    }

    @Override
    public int update(Jurisdiction jurisdiction) {
        if (jurisdiction.getJurisdictionId()==null){
            return 0;
        }
        return jurisdictionMapper.updateByPrimaryKeySelective(jurisdiction);
    }

    @Override
    public int delete(int id) {
        return jurisdictionMapper.deleteByPrimaryKey(id);
    }
}
