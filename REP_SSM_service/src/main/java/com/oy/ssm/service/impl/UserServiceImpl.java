package com.oy.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.oy.ssm.common.GetObject;
import com.oy.ssm.mapper.UserMapper;
import com.oy.ssm.model.User;
import com.oy.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author 勋
 * @ClassName UserServiceImpl
 * @Description
 * @Date 2021-07-13 09:32:37
 * @Version v1.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public boolean selectKey(User user) {
        List<User> users = selectList(user);
        if (users.size()>0){
            return true;
        }
        return false;
    }

    @Override
    public List<User> selectList(User user) {
        return userMapper.selectList(user);
    }

    @Override
    public PageInfo<User> selectPage(int page, int size, User user) {
        PageHelper.startPage(page,size);
        List<User> users = selectList(user);
        return new PageInfo<User>(users);
    }

    @Override
    public PageInfo<User> selectPage(Map<String, String> map) {
        //将map中的数据取出，设置分页
        PageHelper.startPage(Integer.valueOf(map.get("page")),Integer.valueOf(map.get("size")));
        //调用工具类，将map中的数据实体化
        User webObject = GetObject.getWebObject(map, User.class);
        //调用查询方法
        List<User> users = selectList(webObject);
        return new PageInfo<>(users);
    }


    @Override
    public int insert(User user) {
        return userMapper.insertSelective(user);
    }

    @Override
    public int update(User user) {
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public int delete(int id) {
        return userMapper.deleteByPrimaryKey(id);
    }
}
