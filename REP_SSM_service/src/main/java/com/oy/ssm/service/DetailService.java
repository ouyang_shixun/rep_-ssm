package com.oy.ssm.service;

import com.github.pagehelper.PageInfo;
import com.oy.ssm.model.Detail;

import java.util.List;

/**
 * @author 勋
 * @ClassName DetailService
 * @Description
 * @Date 2021-07-13 09:31:15
 * @Version v1.0
 */
public interface DetailService {

    Detail selectKey(int id);

    Detail selectDetail(Detail detail);

    List<Detail> selectList(Detail detail);

    PageInfo<Detail> selectPage(int page,int size,Detail detail);

    int insert(Detail detail);

    int update(Detail detail);

    int delete(int id);
}
