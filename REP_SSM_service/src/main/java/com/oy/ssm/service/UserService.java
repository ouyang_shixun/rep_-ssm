package com.oy.ssm.service;

import com.github.pagehelper.PageInfo;
import com.oy.ssm.model.User;

import java.util.List;
import java.util.Map;

/**
 * @author 勋
 * @ClassName UserService
 * @Description
 * @Date 2021-07-13 09:32:20
 * @Version v1.0
 */
public interface UserService {

    boolean selectKey(User user);

    List<User> selectList(User user);

    PageInfo<User> selectPage(int page,int size,User user);

    PageInfo<User> selectPage(Map<String,String> map);

    int insert(User user);

    int update(User user);

    int delete(int id);
}
