package com.oy.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.oy.ssm.mapper.DetailMapper;
import com.oy.ssm.model.Detail;
import com.oy.ssm.service.DetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 勋
 * @ClassName DetailServiceImpl
 * @Description
 * @Date 2021-07-13 09:31:53
 * @Version v1.0
 */
@Service
public class DetailServiceImpl implements DetailService {
    @Autowired
    DetailMapper detailMapper;

    @Override
    public Detail selectKey(int id) {
        return detailMapper.selectByPrimaryKey(id);
    }

    @Override
    public Detail selectDetail(Detail detail) {
        return detailMapper.selectList(detail).get(0);
    }

    @Override
    public List<Detail> selectList(Detail detail) {
        return detailMapper.selectList(detail);
    }

    @Override
    public PageInfo<Detail> selectPage(int page, int size, Detail detail) {
        PageHelper.startPage(page,size);
        List<Detail> details = selectList(detail);
        return new PageInfo<>(details);
    }

    @Override
    public int insert(Detail detail) {
        return detailMapper.insertSelective(detail);
    }

    @Override
    public int update(Detail detail) {
        return detailMapper.updateByPrimaryKeySelective(detail);
    }

    @Override
    public int delete(int id) {
        return detailMapper.deleteByPrimaryKey(id);
    }
}
