package com.oy.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.oy.ssm.model.Jurisdiction;
import com.oy.ssm.model.PageVo;
import com.oy.ssm.model.User;
import com.oy.ssm.service.DetailService;
import com.oy.ssm.service.JurisdictionService;
import com.oy.ssm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Map;

/**
 * @author 勋
 * @ClassName WebController
 * @Description 访问
 * @Date 2021-07-13 10:46:28
 * @Version v1.0
 */
@Controller
@CrossOrigin
public class WebController {
    @Autowired
    UserService userService;
    @Autowired
    JurisdictionService jurisdictionService;
    @Autowired
    DetailService detailService;

    /**
     * 查询user数据并分页
     * @param map 查询数据时的条件,
     * @return
     */
    @ResponseBody
    @RequestMapping("selectUser")
    public PageVo<User> selectUser(@RequestBody Map<String,String> map){
        PageInfo<User> userPageInfo = userService.selectPage(map);
        long total = userPageInfo.getTotal();
        List<User> list = userPageInfo.getList();
        System.out.println(list);
        return new PageVo<>(total,list);

    }


    @ResponseBody
    @RequestMapping("selectJurisdiction")
    public PageVo<Jurisdiction> selectJurisdiction(@RequestBody Map<String,String> map){
        PageInfo<Jurisdiction> jurisdictionPageInfo = jurisdictionService.selectPage(map);
        long total = jurisdictionPageInfo.getTotal();
        List<Jurisdiction> list = jurisdictionPageInfo.getList();
        return new PageVo<>(total,list);
    }

    @ResponseBody
    @RequestMapping("selectJurisdictionList")
    public List<Jurisdiction> selectJurisdictionList(){
        return jurisdictionService.selectList(null);
    }

    @ResponseBody
    @RequestMapping("updateUser")
    public void updateUser(User user){
        if (user.getUserId()==null){
            userService.insert(user);
        }else {
            userService.update(user);
        }
    }
    @ResponseBody
    @RequestMapping("insertUpdateJur")
    public void insertUpdateJur(Jurisdiction jurisdiction){
        if (jurisdiction.getJurisdictionId()==null){
            jurisdictionService.insert(jurisdiction);
        }else {
            jurisdictionService.update(jurisdiction);
        }
    }
    @ResponseBody
    @RequestMapping("deleteUser")
    public void deleteUser(int  id){
        userService.delete(id);
    }
    @ResponseBody
    @RequestMapping("deleteJurisdiction")
    public void deleteJurisdiction(int  id){
        jurisdictionService.delete(id);
    }
}
