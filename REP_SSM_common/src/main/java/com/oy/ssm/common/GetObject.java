package com.oy.ssm.common;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

/**
 * @author 勋
 * @ClassName GetObject
 * @Description
 * @Date 2021-06-11 09:53:48
 * @Version v1.0
 */
public class GetObject {
    /**
     * 获取数据，实例化对象并返回
     * @param map 前端
     * @param obj 实体类的class
     * @param <T>
     * @return 实例化对象
     */
    public static <T> T getWebObject(Map<String,String> map, Class<T> obj){
        T instance = null;
        try {
            //获取所有字段
            Field[] declaredFields = obj.getDeclaredFields();
            //实例化，获得对象
            instance = obj.newInstance();
            for (Field f :declaredFields){
                // 字段名
                String name = f.getName();
                //这个字段除外
                if(name.equals("serialVersionUID")) {
                    continue;
                }
                //获取到字段的数据类型
                Class<?> type = obj.getDeclaredField(name).getType();
                //将字段的首字母大写
                String invokeMethod  =  name.substring(0,1).toUpperCase()+name.substring(1);
                if(StringUtils.isNotBlank(invokeMethod)) {
                    // 字段的Set赋值方法
                    Method method = obj.getMethod("set" + invokeMethod, type);
                    //从前端获取数据
                    String parameter = map.get(name);
                    if (StringUtils.isNotBlank(parameter)){}else {
                        parameter = null;
                    }
                    //从前端获取的数据是spring类型，需要根据字段类型强制转换
                    if (type.isAssignableFrom(String.class)){
                        method.invoke(instance,parameter);
                    }else if (type.isAssignableFrom(int.class) || type.isAssignableFrom(Integer.class) && StringUtils.isNotBlank(parameter)){
                        method.invoke(instance,Integer.valueOf(parameter));
                    }else if(type.isAssignableFrom(boolean.class) || type.isAssignableFrom(Boolean.class)){
                        method.invoke(instance,Boolean.valueOf(parameter));
                    }else if(type.isAssignableFrom(double.class) || type.isAssignableFrom(Double.class)){
                        method.invoke(instance,Double.valueOf(parameter));
                    }else if(type.isAssignableFrom(Date.class) && StringUtils.isNotBlank(parameter)){
                        DateTime parse = DateUtil.parse(parameter, "yyyy-MM-dd");
                        method.invoke(instance,parse);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }
}
